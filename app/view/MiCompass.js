Ext.define('Ejemplo1Compass.view.MiCompass', {
    extend: 'Ext.form.Panel',
    alias: "widget.micompass",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.*',
        'Ext.field.*'
    ],
    config: {
        width: '100%',
        height: '100%',        
        items: [{
            xtype: 'fieldset',
            title: 'Compass',
            items: [{
                label : 'Head',
                itemId: 'headingId',
                xtype: 'textfield',
                name: 'head'                
            }]
        }]
    }
});
