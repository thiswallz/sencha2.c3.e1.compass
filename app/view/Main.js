Ext.define('Ejemplo1Compass.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Compass',
                iconCls: 'time',

                items: [{
                    xtype : 'toolbar',
                    docked: 'top',
                    items: [{
                        xtype: 'button',
                        ui: 'action',
                        action: 'btnHeading',
                        iconMask: true,
                        iconCls: 'locate'
                    },{
                        xtype: 'button',
                        ui: 'action',
                        text: 'Loop',
                        action: 'btnHeadingLoop',
                        iconMask: true,
                        iconCls: 'locate'
                    },
                    { xtype: "spacer" },
                    {
                        xtype: 'button',
                        ui: 'normal',
                        iconMask: true,
                        iconCls: 'more',
                        handler: function() {
                            var me=this.up('main');
                            if(!this.actions){
                                this.actions = Ext.Viewport.add({
                                    xtype: 'actionsheet',
                                    items: [
                                        {
                                            text: 'Parar Listener',
                                            ui: 'decline',
                                            scope: this,
                                            handler: function() {
                                                me.fireEvent('stopListener', me);
                                                this.actions.hide();
                                            }
                                        },
                                        {
                                            text: 'Cancelar',
                                            xtype: 'button',
                                            scope: this,
                                            handler: function() {
                                                this.actions.hide();
                                            }
                                        }

                                    ]
                                });
                            }else{
                                this.actions.show();
                            }

                        }
                    }]
                    },
                    {
                        xtype: 'micompass'
                    }
                ]
            }
        ]
    }
});
