Ext.define('Ejemplo1Compass.controller.Compass', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        refs: {
            micompass: 'micompass',
            main: 'main'              
        },
        control: {
            'button[action=btnHeading]': {
                tap: 'heading'
            },
            'button[action=btnHeadingLoop]': {
                tap: 'headListener'
            },
            main: {
                stopListener: 'stopListener'
            }                
        }
    },
    launch: function() {
        var watchID = null;
    },
    stopListener: function(){
        if (watchID) {
            navigator.compass.clearWatch(watchID);
            watchID = null;
        }
    },
    heading: function(){
        var view = this.getMicompass();
        navigator.compass.getCurrentHeading(function(heading){

            view.down('#headingId').setValue(heading.magneticHeading);

        }, onError);
    },
    headListener: function(){
        var view = this.getMicompass();
        var options = { frequency: 500 };

        watchID = navigator.compass.watchHeading(function(heading){

            view.down('#headingId').setValue(heading.magneticHeading);

        }, onError, options);


    }
});